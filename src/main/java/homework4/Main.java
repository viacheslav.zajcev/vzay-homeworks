package homework4;

public class Main {
    public static void main(String[] args) {
        Pet mars = new Pet("cat", "Mars", 3, 25, new String[]{"sleep, run"});

        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, mars, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });


        Family family1 = new Family(eva, adam);
        family1.setPet(mars);
        family1.addChild(tom);

        System.out.println(mars);
        System.out.println(adam);
        System.out.println(eva);
        System.out.println(tom);
        System.out.println(family1);
        System.out.printf("Tom's family: %s\n", tom.getFamily());
        System.out.println(tom); // toString() by default
        tom.greetPet();
        mars.respond();
        tom.describePet();
        mars.eat();
        mars.foul();
    }
}
