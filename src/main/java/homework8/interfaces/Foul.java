package homework8.interfaces;

public interface Foul {
    default void foul() {
        System.out.println("\nIt needs to clean up...");
    }
}
