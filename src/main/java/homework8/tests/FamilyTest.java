package homework8.tests;

import homework8.models.Family;
import homework8.models.Human;
import homework8.enums.DayOfWeek;
import homework8.models.pets.*;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.*;

public class FamilyTest {
    @Test
    public void domesticCatToStringTest() {
        Pet pet = new DomesticCat("Nick", 5, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});
        String expected = "Cat{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void roboCatToStringTest() {
        Pet pet = new RoboCat("Nick", 5, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});
        String expected = "Robocat{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void dogToStringTest() {
        Pet pet = new Dog("Nick", 5, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});
        String expected = "Dog{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void fishToStringTest() {
        Pet pet = new Fish("Nick", 5, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});
        String expected = "Fish{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void humanToStringTest() {
        Pet pet = new DomesticCat("Nick", 5, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});
        Human steve = new Human("Steven", "King", 1950, 130, null, null, new HashMap<>());
        String expected = "Human{name='Steven King', year=1950, iq=130}";
        assertEquals(expected, steve.toString());
    }

    @Test
    public void familyToString() {
        Pet mars = new DomesticCat("Mars", 3, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});

        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});

        Family family = new Family(eva, adam);
        family.addChild(tom);
        family.addPet(mars);

        String expected = "Family{mother=Human{name='Eva Walker', year=1994, iq=118}, father=Human{name='Adam Walker', year=1993, iq=120}, children=[Human{name='Tom Walker', year=2018, iq=110}], pet=[Cat{nickname='Mars', age=3, trickLevel=25, habits=[sleep, play]}]}";
        assertEquals(expected, family.toString());
    }

    @Test
    public void addChildTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});


        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});


        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertEquals(family.getChildren().get(0), tom);
    }

    @Test
    public void deleteChildPosTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});


        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertTrue(family.deleteChild(0));
    }

    @Test
    public void deleteChildByIdPosTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});


        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertTrue(family.deleteChild(tom));
    }

    @Test
    public void deleteChildNegTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});


        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertFalse(family.deleteChild(new Human()));
    }

    @Test
    public void deleteChildByIdNegTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});


        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertFalse(family.deleteChild(1));
    }

    @Test
    public void countFamilyTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});


        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});

        Family family = new Family(eva, adam);
        family.addChild(tom);

        int expected = 3;

        assertEquals(expected, family.countFamily());
    }
}
