package homework8.dao;

import homework8.enums.DayOfWeek;
import homework8.models.Family;
import homework8.models.Human;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FamilyStorage {
    private static final List<Family> families = new ArrayList<>() {{
        add(new Family(
                new Human("Alice", "Johnson", 1995, 150, null, null, new HashMap<>() {{
                    put(DayOfWeek.MONDAY, "Office");
                    put(DayOfWeek.TUESDAY, "Fitness");
                    put(DayOfWeek.WEDNESDAY, "Meetup");
                    put(DayOfWeek.THURSDAY, "Study Group");
                    put(DayOfWeek.FRIDAY, "Relaxation");
                }}),
                new Human("James", "Smith", 1975, 120, null, null, new HashMap<>() {{
                    put(DayOfWeek.MONDAY, "Office Work");
                    put(DayOfWeek.TUESDAY, "Fitness Training");
                    put(DayOfWeek.WEDNESDAY, "Team Meeting");
                    put(DayOfWeek.THURSDAY, "Study Time");
                    put(DayOfWeek.FRIDAY, "Weekend Prep");
                }})
        ));
        add(new Family(
                new Human("Mary", "Smith", 1975, 120, null, null, new HashMap<>() {{
                    put(DayOfWeek.MONDAY, "Office Work");
                    put(DayOfWeek.TUESDAY, "Fitness Training");
                    put(DayOfWeek.WEDNESDAY, "Team Meeting");
                    put(DayOfWeek.THURSDAY, "Study Time");
                    put(DayOfWeek.FRIDAY, "Weekend Prep");
                }}),
                new Human("James", "Johnson", 1985, 130, null, null, new HashMap<>() {{
                    put(DayOfWeek.MONDAY, "Office Work");
                    put(DayOfWeek.TUESDAY, "Fitness Training");
                    put(DayOfWeek.WEDNESDAY, "Team Meeting");
                    put(DayOfWeek.THURSDAY, "Study Time");
                    put(DayOfWeek.FRIDAY, "Weekend Prep");
                }})
        ));
    }};

    public static List<Family> getFamilies() {
        return families;
    }
}
