package homework8.models;

import homework8.enums.DayOfWeek;

import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int yearOfBirth;
    private int iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;

    public Human() {
    }

    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    public Human(String name, String surname, int yearOfBirth, Woman mother, Man father) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    public Human(String name, String surname, int yearOfBirth, int iq, Human mother, Human father, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPet() {
        System.out.printf("\nHello, %s!", Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getNickname());
    }

    public void describePet() {
        System.out.printf("\nI have a %s. It is %d y.o. It is %s very tricky.",
                Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getSpecies().getAlias(),
                Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getAge(),
                Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getTrickLevel() > 50 ? "" : "not");
    }

    private String getFullName() {
        return String.format("%s %s", name, surname);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return String.format("Human{name='%s', year=%s, iq=%d}", this.getFullName(), yearOfBirth, iq);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return yearOfBirth == human.yearOfBirth && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && schedule.equals(human.schedule) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, yearOfBirth, iq, family);
        result = 31 * result + schedule.hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human object is being finalized: " + this);
        super.finalize();
    }

    public int getAge() {
        int currentYear = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear();
        return currentYear - this.yearOfBirth;
    }
}
