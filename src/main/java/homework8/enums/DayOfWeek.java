package homework8.enums;

public enum DayOfWeek {
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");

    private final String alias;

    DayOfWeek(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}
