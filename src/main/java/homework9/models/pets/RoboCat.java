package homework9.models.pets;

import homework9.enums.Species;
import homework9.interfaces.Foul;

import java.util.HashSet;

public class RoboCat extends Pet implements Foul {
    public RoboCat(String nickname) {
        this.setNickname(nickname);
        this.setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
        this.setSpecies(Species.ROBO_CAT);
    }
    @Override
    public void respond()
    {
        System.out.printf("\nHello, master. I am %s. I missed you!", this.getNickname());
    }
}
