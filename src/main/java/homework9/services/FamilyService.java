package homework9.services;

import homework9.dao.CollectionFamilyDao;
import homework9.dao.FamilyDao;
import homework9.models.Family;
import homework9.models.Human;
import homework9.models.pets.Pet;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    public FamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = familyDao.getAllFamilies();

        allFamilies.forEach(family -> System.out.printf("%d: %s\n", allFamilies.indexOf(family), family));
    }

    public List<Family> getFamiliesBiggerThan(int value) {
        return familyDao.getAllFamilies().stream().filter(family -> family.countFamily() > value).collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int value) {
        return familyDao.getAllFamilies().stream().filter(family -> family.countFamily() < value).collect(Collectors.toList());
    }

    public long countFamiliesWithMemberNumber(int value) {
        return familyDao.getAllFamilies().stream().filter(family -> family.countFamily() == value).count();
    }

    public void createNewFamily(Human mother, Human father) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public void deleteFamilyByIndex (int index) {
        familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String name, String surname) {
        long birthDate = new Date().getTime();
        family.addChild(new Human(name, surname, birthDate));
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyDao.getAllFamilies().stream()
                .filter(family -> !family.getChildren().isEmpty())
                .forEach(family -> {
                    List<Human> childrenToRemove = family.getChildren().stream()
                            .filter(child -> child.getAge() > age)
                            .collect(Collectors.toList());

                    childrenToRemove.forEach(family::deleteChild);

                    familyDao.saveFamily(family);
                });
    }



    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public List<Pet> getPets(int familyIndex) {
        return familyDao.getFamilyByIndex(familyIndex).getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        Family family = familyDao.getFamilyByIndex(familyIndex);
        family.addPet(pet);
        familyDao.saveFamily(family);
    }
}
