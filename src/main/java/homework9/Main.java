package homework9;

import homework9.controllers.FamilyController;
import homework9.models.Human;
import homework9.models.pets.Dog;
import homework9.models.pets.DomesticCat;

import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();

        familyController.bornChild(familyController.getFamilyById(0), "Tom", "Smith");
        familyController.bornChild(familyController.getFamilyById(0), "Alex", "Smith");
        familyController.bornChild(familyController.getFamilyById(0), "Abigale", "Smith");

        Human childToAdopt = new Human("Marry", "Piterson", "02/03/1998", 1990);

        familyController.adoptChild(familyController.getFamilyById(1), childToAdopt);

        familyController.addPet(0, new DomesticCat("Mars", 3, 25, new HashSet<>() {{
            add("sleep");
            add("scratch");
        }}));
        familyController.addPet(0, new Dog("Jack", 2, 55, new HashSet<>() {{
            add("bark");
            add("run");
        }}));

        familyController.createNewFamily(familyController.getFamilyById(1).getChildren().get(0), familyController.getFamilyById(0).getChildren().get(0));

        familyController.deleteFamilyByIndex(2);

        System.out.printf("First family's pets: %s\n", familyController.getFamilyById(0).getPets());

        System.out.printf("Family get pet: %s\n", familyController.getFamilyById(0));

        System.out.printf("Families bigger than 4 members: %s\n", familyController.getFamiliesBiggerThan(4));
        System.out.printf("Families less than 4 members: %s\n", familyController.getFamiliesLessThan(4));

        familyController.deleteAllChildrenOlderThan(18);

        System.out.printf("Number of families: %d\n", familyController.count());

        System.out.println(familyController.getAllFamilies());

        familyController.displayAllFamilies();
    }
}
