package homework9.controllers;

import homework9.models.Family;
import homework9.models.Human;
import homework9.models.pets.Pet;
import homework9.services.FamilyService;

import java.util.List;

public class FamilyController {
    public FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int value) {
        return familyService.getFamiliesBiggerThan(value);
    }

    public List<Family> getFamiliesLessThan(int value) {
        return familyService.getFamiliesLessThan(value);
    }

    public long countFamiliesWithMemberNumber(int value) {
        return familyService.countFamiliesWithMemberNumber(value);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    public void deleteFamilyByIndex (int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String name, String surname) {
        return familyService.bornChild(family,name, surname);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public List<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }
}
