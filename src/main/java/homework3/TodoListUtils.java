package homework3;

public class TodoListUtils {
    public static String[][] initSchedule() {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do homework";

        schedule[1][0] = "Monday";
        schedule[1][1] = "work from 9 AM to 5 PM";

        schedule[2][0] = "Tuesday";
        schedule[2][1] = "gym in the evening";

        schedule[3][0] = "Wednesday";
        schedule[3][1] = "meeting with friends";

        schedule[4][0] = "Thursday";
        schedule[4][1] = "grocery shopping";

        schedule[5][0] = "Friday";
        schedule[5][1] = "movie night";

        schedule[6][0] = "Saturday";
        schedule[6][1] = "practice coding";

        return schedule;
    }

    public static String getTaskByDay(String[][] schedule, String day) {
        String task = "";
        for (String[] strings : schedule)
            if (strings[0].equalsIgnoreCase(day)) {
                task = strings[1];
            }
        return task;
    }

    public static boolean isProperInput(String[][] schedule, String day) {
        for (String[] strings : schedule) {
            if (strings[0].equalsIgnoreCase(day)) {
                return true;
            }
        }

        return false;
    }

    public static String formatDay(String day) {
        return day.substring(0, 1).toUpperCase() + day.substring(1).toLowerCase();
    }
}
