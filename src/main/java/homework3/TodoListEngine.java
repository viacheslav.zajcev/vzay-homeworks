package homework3;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class TodoListEngine {
    public static void start() {
        InputStream in = System.in;
        PrintStream out = System.out;
        Scanner sc = new Scanner(in);
        String[][] schedule = TodoListUtils.initSchedule();

        while (true) {
            out.print("\nPlease, input the day of the week: ");
            String day = sc.next();
            if (day.equalsIgnoreCase("exit")) break;
            if (TodoListUtils.isProperInput(schedule, day)) {
                out.printf("Your tasks for %s: %s\n", TodoListUtils.formatDay(day), TodoListUtils.getTaskByDay(schedule, day));
            } else {
                out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}
