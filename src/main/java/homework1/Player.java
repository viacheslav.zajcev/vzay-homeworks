package homework1;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Player {
    private final PrintStream out = System.out;
    private final InputStream in = System.in;
    private final Scanner sc = new Scanner(in);
    private String name;

    public void askName() {
        out.print("Enter your name: ");
        name = sc.next();
    }

    public void greeting() {
        out.printf("\nHello, %s!", this.name);
    }

    public void congrats() {
        out.printf("\nCongratulations, %s!\n", this.name);
    }
}

