package homework1;

import java.io.PrintStream;
import java.util.Random;

public class NumberGameUtils {
    private final PrintStream out = System.out;

    public void printGameHeader() {
        out.println("\nGUESS THE NUMBER GAME\n");
    }

    public void letsBegin() {
        out.println("\nLet the game begin!");
        out.println("Guess the number between 0 and 100\n");
    }

    public int getRandomNumber() {
        return new Random().nextInt(100);
    }

    public int toInt(String raw) {
        return Integer.parseInt(raw);
    }

    public boolean isInt(String raw) {
        try {
            Integer.parseInt(raw);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public boolean validate(int x) {
        return x >= 0 && x <= 100;
    }

    public int[] addElement(int[] array, int element) {
        int[] newArray = new int[array.length + 1];
        System.arraycopy(array, 0, newArray, 0, array.length);
        newArray[array.length] = element;
        return newArray;
    }

    public int[] bubbleSortDescending(int[] array) {
        int n = array.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (array[j] < array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }

    public void printArray(int[] array) {
        out.print("\nYour numbers: ");
        for (int el : array) {
            out.printf("%d ", el);
        }
    }
}
