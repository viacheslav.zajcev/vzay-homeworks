package homework1;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class NumbersGameEngine {
    private final PrintStream out = System.out;
    private final InputStream in = System.in;
    private final Scanner sc = new Scanner(this.in);
    private final NumberGameUtils ngu = new NumberGameUtils();
    private int lastGuessedNumber;
    private int[] playerNumbers = new int[0];

    private void initLastGuessedNumber() {
        lastGuessedNumber = ngu.getRandomNumber();
    }

    public void start() {
        int guessInt;
        Player player = new Player();

        ngu.printGameHeader();
        player.askName();
        player.greeting();
        ngu.letsBegin();

        initLastGuessedNumber();

        while (true) {
            out.print("Enter the number: ");
            String guess = sc.nextLine();
            if (ngu.isInt(guess)) {
                guessInt = ngu.toInt(guess);
                if (ngu.validate(guessInt)) {
                    playerNumbers = ngu.addElement(playerNumbers, guessInt);
                    if (guessInt < lastGuessedNumber) out.println("Your number is too small. Please, try again.");
                    if (guessInt > lastGuessedNumber) out.println("Your number is too big. Please, try again.");
                    if (guessInt == lastGuessedNumber) {
                        break;
                    }
                }
                else out.println("The number should be between 0 and 100.");
            } else out.println("Not number given.");
        }
        ngu.printArray(ngu.bubbleSortDescending(playerNumbers));
        player.congrats();
    }

}
