package homework6;

public final class Woman extends Human {
    @Override
    public void greetPet() {
        System.out.printf("\nHey, %s!", this.getFamily().getPet().getNickname());
    }

    public void makeup() {
        System.out.println("\n Ready! I am beautiful!");
    }
}
