package homework6.interfaces;

public interface Foul {
    default void foul() {
        System.out.println("\nIt needs to clean up...");
    }
}
