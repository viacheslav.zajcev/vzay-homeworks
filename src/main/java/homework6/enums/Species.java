package homework6.enums;

public enum Species {
    DOG("Dog"),
    FISH("Fish"),
    DOMESTIC_CAT("Cat"),
    ROBO_CAT("Robocat"),
    UNKNOWN("Something");

    private final String alias;

    Species(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}
