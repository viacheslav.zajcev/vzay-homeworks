package homework6;

import homework6.pets.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private static final int MIN_FAMILY_LENGTH = 2;
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        mother.setFamily(this);
        this.father = father;
        father.setFamily(this);
    }

    public void addChild(Human child) {
        Human[] newChildren = new Human[children.length + 1];
        System.arraycopy(children, 0, newChildren, 0, children.length);
        newChildren[children.length] = child;
        children = newChildren;
        child.setFamily(this);
    }

    public boolean deleteChild(Human child) {
        int childIndex = -1;
        for (int i = 0; i < children.length; i++) {
            if (children[i] == child) {
                childIndex = i;
                break;
            }
        }

        if (childIndex != - 1) {
            return deleteChild(childIndex);
        } else return false;
    }

    public boolean deleteChild(int index) {
        if (index >= 0 && index < children.length) {
            Human[] newChildren = new Human[children.length - 1];
            int newIndex = 0;
            for (int i = 0; i < children.length - 1; i++) {
                if (i != index) {
                    newChildren[newIndex] = children[i];
                    newIndex++;
                }
            }
            children = newChildren;
            return true;
        } else return false;
    }

    public int countFamily() {
        return MIN_FAMILY_LENGTH + children.length;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return String.format("Family{mother=%s, father=%s, children=%s, pet=%s}", mother.toString(), father.toString(), Arrays.toString(children), pet);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family object is being finalized: " + this);
        super.finalize();
    }
}
