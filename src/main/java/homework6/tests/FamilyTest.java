package homework6.tests;

import homework6.Family;
import homework6.Human;
import homework6.pets.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyTest {
    @Test
    public void domesticCatToStringTest() {
        Pet pet = new DomesticCat("Nick", 5, 25, new String[]{"sleep, play"});
        String expected = "Cat{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void roboCatToStringTest() {
        Pet pet = new RoboCat("Nick", 5, 25, new String[]{"sleep, play"});
        String expected = "Robocat{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void dogToStringTest() {
        Pet pet = new Dog("Nick", 5, 25, new String[]{"sleep, play"});
        String expected = "Dog{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void fishToStringTest() {
        Pet pet = new Fish("Nick", 5, 25, new String[]{"sleep, play"});
        String expected = "Fish{nickname='Nick', age=5, trickLevel=25, habits=[sleep, play]}";
        assertEquals(expected, pet.toString());
    }

    @Test
    public void humanToStringTest() {
        Pet pet = new DomesticCat("Nick", 5, 25, new String[]{"sleep, play"});
        Human steve = new Human("Steven", "King", 1950, 130, pet, null, null, new String[][]{});
        String expected = "Human{name='Steven King', year=1950, iq=130}";
        assertEquals(expected, steve.toString());
    }

    @Test
    public void familyToString() {
        Pet mars = new DomesticCat("Mars", 3, 25, new String[]{"sleep, run"});

        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, mars, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family = new Family(eva, adam);
        family.setPet(mars);
        family.addChild(tom);

        String expected = "Family{mother=Human{name='Eva Walker', year=1994, iq=118}, father=Human{name='Adam Walker', year=1993, iq=120}, children=[Human{name='Tom Walker', year=2018, iq=110}], pet=Cat{nickname='Mars', age=3, trickLevel=25, habits=[sleep, run]}}";
        assertEquals(expected, family.toString());
    }

    @Test
    public void addChildTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, null, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertEquals(family.getChildren()[0], tom);
    }

    @Test
    public void deleteChildPosTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, null, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertTrue(family.deleteChild(0));
    }

    @Test
    public void deleteChildByIdPosTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, null, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertTrue(family.deleteChild(tom));
    }

    @Test
    public void deleteChildNegTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, null, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertFalse(family.deleteChild(new Human()));
    }

    @Test
    public void deleteChildByIdNegTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, null, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family = new Family(eva, adam);
        family.addChild(tom);

        assertFalse(family.deleteChild(1));
    }

    @Test
    public void countFamilyTest() {
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new String[][]{
                {"Sunday", "do homework"},
                {"Monday", "work from 9 AM to 5 PM"},
                {"Tuesday", "gym in the evening"},
                {"Wednesday", "meeting with friends"},
                {"Thursday", "grocery shopping"},
                {"Friday", "movie night"},
                {"Saturday", "practice coding"}
        });

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new String[][]{
                {"Sunday", "relax"},
                {"Monday", "work from home"},
                {"Tuesday", "gym in the morning"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "shopping"},
                {"Friday", "dinner with friends"},
                {"Saturday", "hiking trip"}
        });

        Human tom = new Human("Tom", "Walker", 2018, 110, null, eva, adam, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family = new Family(eva, adam);
        family.addChild(tom);

        int expected = 3;

        assertEquals(expected, family.countFamily());
    }
}
