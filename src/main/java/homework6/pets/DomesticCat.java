package homework6.pets;

import homework6.enums.Species;
import homework6.interfaces.Foul;

public class DomesticCat extends Pet implements Foul {
    public DomesticCat(String nickname) {
        this.setNickname(nickname);
        this.setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
        this.setSpecies(Species.DOMESTIC_CAT);
    }
    @Override
    public void respond()
    {
        System.out.printf("\nHello, master. I am %s. I missed you!", this.getNickname());
    }
}
