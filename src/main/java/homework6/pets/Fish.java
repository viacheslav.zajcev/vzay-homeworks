package homework6.pets;

import homework6.enums.Species;

public class Fish extends Pet {
    public Fish(String nickname) {
        this.setNickname(nickname);
        this.setSpecies(Species.FISH);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
        this.setSpecies(Species.FISH);
    }
    @Override
    public void respond() {
        System.out.println("\n....");
    }
}
