package homework10.dao;

import homework10.models.Family;

import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private final List<Family> families = FamilyStorage.getFamilies();

    public CollectionFamilyDao () {

    }
    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        return families.remove(index) != null;
    }

    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)) {
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }
    }
}
