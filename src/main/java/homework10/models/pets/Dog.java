package homework10.models.pets;

import homework10.enums.Species;
import homework10.interfaces.Foul;

import java.util.HashSet;

public class Dog extends Pet implements Foul {
    public Dog(String nickname) {
        this.setNickname(nickname);
        this.setSpecies(Species.DOG);
    }

    public Dog(String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
        this.setSpecies(Species.DOG);
    }
    @Override
    public void respond()
    {
        System.out.printf("\nHello, master. I am %s. I missed you!", this.getNickname());
    }
}
