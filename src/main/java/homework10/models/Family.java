package homework10.models;

import homework10.models.pets.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Family {
    private static final int MIN_FAMILY_LENGTH = 2;
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private List<Pet> pets = new ArrayList<>();

    public Family(Human mother, Human father) {
        this.mother = mother;
        mother.setFamily(this);
        this.father = father;
        father.setFamily(this);
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public boolean deleteChild(int index) {
        try {
            children.remove(index);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void addPet(Pet pet) {
        this.getPets().add(pet);
    }

    public int countFamily() {
        return MIN_FAMILY_LENGTH + children.size();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setPet(List<Pet> pet) {
        this.pets = pet;
    }

    @Override
    public String toString() {
        return String.format("Family{mother=%s, father=%s, children=%s, pet=%s}", mother.toString(), father.toString(), children.toString(), pets);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && children.equals(family.children) && Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pets);
        result = 31 * result + children.hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family object is being finalized: " + this);
        super.finalize();
    }
}
