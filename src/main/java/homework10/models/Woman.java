package homework10.models;

import java.util.Objects;

public final class Woman extends Human {
    @Override
    public void greetPet() {
        System.out.printf("\nHey, %s!", Objects.requireNonNull(this.getFamily().getPets().stream().findFirst().orElse(null)).getNickname());
    }

    public void makeup() {
        System.out.println("\n Ready! I am beautiful!");
    }
}
