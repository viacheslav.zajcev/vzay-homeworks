package homework10.models;

import homework10.enums.DayOfWeek;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;

    public Human() {
    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, long birthDate, Woman mother, Man father) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        try {
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException e) {
            System.err.printf("%s. Wrong date provided.", e.getMessage());
        }
        this.iq = iq;
    }

    public Human(String name, String surname, long birthDate, int iq, Human mother, Human father, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPet() {
        System.out.printf("\nHello, %s!", Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getNickname());
    }

    public void describePet() {
        System.out.printf("\nI have a %s. It is %d y.o. It is %s very tricky.",
                Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getSpecies().getAlias(),
                Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getAge(),
                Objects.requireNonNull(family.getPets().stream().findFirst().orElse(null)).getTrickLevel() > 50 ? "" : "not");
    }

    private String getFullName() {
        return String.format("%s %s", name, surname);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return String.format("Human{name='%s', birthDate=%s, iq=%d}", this.getFullName(), getBirthDateString(), iq);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && schedule.equals(human.schedule) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, birthDate, iq, family);
        result = 31 * result + schedule.hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human object is being finalized: " + this);
        super.finalize();
    }

    public int getAge() {
        int currentYear = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear();
        return currentYear - new Date(birthDate).getYear();
    }

    public String getBirthDateString() {
        return new SimpleDateFormat("dd/MM/yyyy").format(new Date(this.birthDate));
    }

    public String describeAge() {
        Instant instant = Instant.ofEpochMilli(birthDate);
        LocalDate birthLocalDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentDate = LocalDate.now();

        Period age = Period.between(birthLocalDate, currentDate);
        int years = age.getYears();
        int months = age.getMonths();
        int days = age.getDays();

        return String.format("%d years, %d months, %d days", years, months, days);
    }
}
