package homework10.tests;

import homework10.dao.FamilyStorage;
import homework10.models.Family;
import homework10.models.Human;
import homework10.models.pets.DomesticCat;
import homework10.models.pets.Pet;
import homework10.services.FamilyService;
import org.junit.Before;
import org.junit.Test;

import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;

public class FamilyServiceTest {
    private FamilyService familyService;

    @Before
    public void setup() {
        familyService = new FamilyService();
    }

    @Test
    public void getFamiliesLessThan() {
        int expected = 1;
        List<Family> familiesBiggerThan = familyService.getFamiliesLessThan(3);
        assertEquals(expected, familiesBiggerThan.size());
    }

    @Test
    public void countFamiliesWithMemberNumber() {
        int expected = 1;
        long familiesBiggerThan = familyService.countFamiliesWithMemberNumber(2);
        assertEquals(expected, familiesBiggerThan);
    }

    @Test
    public void getAllFamilies() {
        List<Family> expectedFamilies = FamilyStorage.getFamilies();
        List<Family> fetchedFamilies = familyService.getAllFamilies();
        assertEquals(expectedFamilies, fetchedFamilies);
    }

    @Test
    public void getFamiliesBiggerThan() {
        List<Family> expectedFamilies = FamilyStorage.getFamilies();
        List<Family> familiesBiggerThan = familyService.getFamiliesBiggerThan(1);
        assertEquals(expectedFamilies, familiesBiggerThan);
    }

    @Test
    public void deleteFamilyByIndex() {
        List<Family> allFamilies = familyService.getAllFamilies();
        familyService.deleteFamilyByIndex(allFamilies.size() - 1);

        int expectedLength = 1;

        int familiesSizeAfterDeletion = familyService.getAllFamilies().size();

        assertEquals(expectedLength, familiesSizeAfterDeletion);
    }

    @Test
    public void bornChild() {
        Family familyBeforeBornChild = familyService.getFamilyById(0);
        int expectedFamilyLength = familyBeforeBornChild.countFamily() + 1;
        Family familyAfterBornChild = familyService.bornChild(familyBeforeBornChild, "Test", "Child");

        int expectedYearOfBirth = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear();

        List<Human> children = familyAfterBornChild.getChildren();
        int actualCreatedChildAge = children.get(children.size() - 1).getAge();

        assertEquals(expectedFamilyLength, familyAfterBornChild.countFamily());
    }

    @Test
    public void adoptChild() {
        Human childToAdopt = new Human("Adopted", "Child", 1980);

        Family family = familyService.getFamilyById(0);
        Family familyAfterAdopt = familyService.adoptChild(family, childToAdopt);

        assertTrue(familyAfterAdopt.getChildren().contains(childToAdopt));
    }

    @Test
    public void deleteAllChildrenOlderThan() {
        Human childToAdopt = new Human("Adopted", "Child", 1980);
        Family family = familyService.getFamilyById(0);
        familyService.adoptChild(family, childToAdopt);

        familyService.deleteAllChildrenOlderThan(18);

        Family familyAfterDeletionByAge = familyService.getFamilyById(0);
        assertFalse(familyAfterDeletionByAge.getChildren().contains(childToAdopt));
    }

    @Test
    public void count() {
        long expectedNumberOfFamilies = familyService.getAllFamilies().size();

        assertEquals(expectedNumberOfFamilies, familyService.count());
    }

    @Test
    public void getFamilyById() {
        Human mother = new Human("Alice", "Johnson", 1985);
        Human father = new Human("Bob", "Smith", 1980);

        familyService.createNewFamily(mother, father);

        Family familyById = familyService.getFamilyById(familyService.count() - 1);

        Family expectedFamily = new Family(mother, father);

        assertEquals(expectedFamily, familyById);
    }

    @Test
    public void getPets() {
        Pet petToAdd = new DomesticCat("Nick", 5, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});

        familyService.addPet(0, petToAdd);

        List<Pet> actualPets = familyService.getPets(0);

        assertTrue(actualPets.contains(petToAdd));
    }

    @Test
    public void addPet() {
        Pet petToAdd = new DomesticCat("Nick", 5, 25, new HashSet<>() {{
            add("sleep");
            add("play");
        }});

        familyService.addPet(0, petToAdd);

        Family family = familyService.getFamilyById(0);
        Pet addedPet = family.getPets().get(0);

        assertEquals(petToAdd, addedPet);
    }
}