package homework7.pets;

import homework7.enums.Species;
import homework7.interfaces.Foul;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {
    public DomesticCat(String nickname) {
        this.setNickname(nickname);
        this.setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
        this.setSpecies(Species.DOMESTIC_CAT);
    }
    @Override
    public void respond()
    {
        System.out.printf("\nHello, master. I am %s. I missed you!", this.getNickname());
    }
}
