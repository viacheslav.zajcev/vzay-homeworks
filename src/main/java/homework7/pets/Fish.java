package homework7.pets;

import homework7.enums.Species;

import java.util.Set;

public class Fish extends Pet {
    public Fish(String nickname) {
        this.setNickname(nickname);
        this.setSpecies(Species.FISH);
    }

    public Fish(String nickname, int age, int trickLevel, Set habits) {
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
        this.setSpecies(Species.FISH);
    }
    @Override
    public void respond() {
        System.out.println("\n....");
    }
}
