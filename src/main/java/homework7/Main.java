package homework7;

import homework7.enums.DayOfWeek;
import homework7.pets.DomesticCat;

import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        DomesticCat mars = new DomesticCat("Mars", 3, 25, new HashSet<String>() {{
            add("sleep");
            add("play");
            add("scratch");
        }});
        Human adam = new Human();
        adam.setName("Adam");
        adam.setSurname("Walker");
        adam.setIq(120);
        adam.setYearOfBirth(1993);
        adam.setSchedule(new HashMap<>() {{
            put(DayOfWeek.MONDAY, "Morning Meeting");
            put(DayOfWeek.TUESDAY, "Gym Time");
            put(DayOfWeek.WEDNESDAY, "Work from Home");
            put(DayOfWeek.THURSDAY, "Project Review");
            put(DayOfWeek.FRIDAY, "Team Lunch");
            put(DayOfWeek.SATURDAY, "Family Time");
            put(DayOfWeek.SUNDAY, "Relaxation");
        }});

        Human eva = new Human("Eva", "Walker", 1994);
        eva.setIq(118);
        eva.setSchedule(new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "relax");
            put(DayOfWeek.MONDAY, "work from home");
            put(DayOfWeek.TUESDAY, "gym in the morning");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "shopping");
            put(DayOfWeek.FRIDAY, "dinner with friends");
            put(DayOfWeek.SATURDAY, "hiking trip");
        }});

        Human tom = new Human("Tom", "Walker", 2018, 110, null, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});


        Family family1 = new Family(eva, adam);
        family1.setPet(mars);
        family1.addChild(tom);

        System.out.println(mars);
        System.out.println(adam);
        System.out.println(eva);
        System.out.println(tom);
        System.out.println(family1);
        System.out.printf("Tom's family: %s\n", tom.getFamily());
        System.out.println(tom); // toString() by default
        tom.greetPet();
        mars.respond();
        tom.describePet();
        mars.eat();
        mars.foul();

//        for (int i = 0; i < 100000; i++) {
//            new Human("Tom", "Shelby", 1850);
//
//            if (i % 10000 == 0) {
//                System.gc();
//            }
//        }
        Human steve = new Human("Steven", "King", 1950, 130, mars, eva, adam, new HashMap<>() {{
            put(DayOfWeek.SUNDAY, "play in the park");
            put(DayOfWeek.MONDAY, "visit grandparents");
            put(DayOfWeek.TUESDAY, "reading");
            put(DayOfWeek.WEDNESDAY, "doctor's appointment");
            put(DayOfWeek.THURSDAY, "playing PC games");
            put(DayOfWeek.FRIDAY, "meeting with friends");
            put(DayOfWeek.SATURDAY, "go for a walk with the parents");
        }});
        System.out.println(steve);
    }
}
