package homework5.enums;

public enum Species {
    DOG,
    CAT,
    BIRD,
    FISH,
    RABBIT,
    OTHER
}
